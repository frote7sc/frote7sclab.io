import { createRouter, createWebHistory } from 'vue-router'

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "home" */ '../views/Home.vue')
    },
    {
      path: '/auth',
      name: 'auth',
      component: () => import(/* webpackChunkName: "auth" */ '../views/Auth.vue')
    },
    /* region Member-only pages */
    {
      path: '/member',
      name: 'member-home',
      component: () => import(/* webpackChunkName: "member-home" */ '../views/members/MemberHome.vue')
    },
    {
      path: '/member/competitions',
      name: 'competitions',
      component: () => import(/* webpackChunkName: "competitions" */ '../views/members/Competitions.vue')
    },
    /* endregion */
    /* region Admin-only pages */
    {
      path: '/admin',
      name: 'admin-home',
      //@ts-ignore
      component: () => import(/* webpackChunkName: "admin-home" */ '../views/admin/AdminHome.vue')
    },
    {
      path: '/admin/winners-club-contracts',
      name: 'winners-club',
      //@ts-ignore
      component: () => import(/* webpackChunkName: "winners-club-contracts" */ '../views/admin/WinnersClubContracts.vue')
    },
    {
      path: '/admin/competitions',
      name: 'admin-competitions',
      component: () => import(/* webpackChunkName: "admin-competition-submissions" */ '../views/admin/CompetitionHome.vue')
    },
    {
      path: '/admin/competitions/:competitionId',
      props: true,
      name: 'view-competition-submissions',
      component: () => import(/* webpackChunkName: "view-competition-submissions" */ '../views/admin/ViewCompetitionSubmissions.vue')
    },
    {
      path: '/admin/short-links',
      name: 'short-links',
      component: () => import(/* webpackChunkName: "short-links" */ '../views/admin/ShortLinks.vue')
    },
    /* endregion */
    {
      path: '/:shortLinkKey',
      props: true,
      name: 'short-link-route',
      component: () => import(/* webpackChunkName: "short-link-route" */ '../views/ShortLinkRouter.vue')
    }
  ]
})

export default router
