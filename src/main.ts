import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios';
import 'vue-datepicker-next/index.css';

const app = createApp(App)

app.use(router);

const port = document.location.port ? `:${document.location.port}` : '';
app.config.globalProperties.$vueDomain = `${document.location.protocol}//${window.location.hostname}${port}`;
app.config.globalProperties.$apiDomain = window.location.hostname === 'localhost' ?
    'https://localhost:7103/api' :
    'https://api.f7sc.com/api';
app.config.globalProperties.$http = axios;

app.mount('#app')
